// Copyright (C) 2015 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file NatNetClientTests.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 1.0
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the NatNet Qt Library.
//
//                          License Agreement
//                     For the NatNet Qt Library
//
//    NatNet Qt Library is an open source library used to directly
//    communicate to a NatNet server.
//
//    NatNet Qt Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    NatNet Qt Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#include "NatNetClient.h"

#include <gmock/gmock.h>


class NatNetClient_getServerDescription : public ::testing::Test
{
public:

    NatNetClient natNetClient;

    void SetUp(){
    }
};

//Test if host present and accounted for
TEST_F(NatNetClient_getServerDescription, Host_Present) {
    sServerDescription serverDescription;
    natNetClient.getServerDescription(&serverDescription);
    ASSERT_THAT(serverDescription.hostPresent, testing::Eq(true));
}
