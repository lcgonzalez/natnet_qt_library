#ifndef NATNETTYPES_H
#define NATNETTYPES_H

// model limits
#define MAX_MODELS                  200     // maximum number of MarkerSets
#define MAX_RIGIDBODIES             1000    // maximum number of RigidBodies
#define MAX_NAMELENGTH              256     // maximum length for strings
#define MAX_MARKERS                 200     // maximum number of markers per MarkerSet
#define MAX_RBMARKERS               20      // maximum number of markers per RigidBody
#define MAX_SKELETONS               100     // maximum number of skeletons
#define MAX_SKELRIGIDBODIES         200     // maximum number of RididBodies per Skeleton
#define MAX_LABELED_MARKERS         1000    // maximum number of labeled markers per frame
#define MAX_UNLABELED_MARKERS       1000    // maximum number of unlabeled (other) markers per frame

#define MAX_PACKETSIZE				100000	// max size of packet (actual packet size is dynamic)

// Client/server message ids
#define NAT_PING                    0
#define NAT_PINGRESPONSE            1
#define NAT_REQUEST                 2
#define NAT_RESPONSE                3
#define NAT_REQUEST_MODELDEF        4
#define NAT_MODELDEF                5
#define NAT_REQUEST_FRAMEOFDATA     6
#define NAT_FRAMEOFDATA             7
#define NAT_MESSAGESTRING           8
#define NAT_UNRECOGNIZED_REQUEST    100

#define UNDEFINED                   999999.9999

// Mocap server application description
typedef struct
{
    bool hostPresent;                       // host is present and accounted for
    char szHostComputerName[MAX_NAMELENGTH];// host computer name
    unsigned char hostComputerAddress[4];   // host IP address
    char szHostApp[MAX_NAMELENGTH];         // name of host app
    unsigned char hostAppVersion[4];        // version of host app
    unsigned char NatNetVersion[4];         // host app's version of NatNet

} sServerDescription;

#endif // NATNETTYPES_H
