// Copyright (C) 2015 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file ClientCore.h
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 1.0
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the NatNet Qt Library.
//
//                          License Agreement
//                     For the NatNet Qt Library
//
//    NatNet Qt Library is an open source library used to directly
//    communicate to a NatNet server.
//
//    NatNet Qt Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    NatNet Qt Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#ifndef NETNAT_H
#define NETNAT_H

#include <QObject>
#include <QHostAddress>
#include <QUdpSocket>
#include <list>
#include <map>

#define MAX_NAMELENGTH              256

// NATNET message ids
#define NAT_PING                    0
#define NAT_PINGRESPONSE            1
#define NAT_REQUEST                 2
#define NAT_RESPONSE                3
#define NAT_REQUEST_MODELDEF        4
#define NAT_MODELDEF                5
#define NAT_REQUEST_FRAMEOFDATA     6
#define NAT_FRAMEOFDATA             7
#define NAT_MESSAGESTRING           8
#define NAT_UNRECOGNIZED_REQUEST    100
#define UNDEFINED                   999999.9999

#define MAX_PACKETSIZE				100000	// max size of packet (actual packet size is dynamic)

class ClientCore : public QObject
{
    Q_OBJECT
public:
    // Avoids structure padding, but not portable. WIthour this, the sSender
    // structure fields cannot be accessed directly.
    // It is better to manually access the Versions numbers from a pointer.
    // See setVersions method for more info.
    //#pragma pack(push, 1) // Strat structures without padding

        // sender
        struct sSender
        {
            char szName[MAX_NAMELENGTH];            // sending app's name
            unsigned char Version[4];               // sending app's version [major.minor.build.revision]
            unsigned char NatNetVersion[4];         // sending app's NatNet version [major.minor.build.revision]

        };

        struct sPacket
        {
            unsigned short iMessage;                // message ID (e.g. NAT_FRAMEOFDATA)
            unsigned short nDataBytes;              // Num bytes in payload
            union
            {
                unsigned char  cData[MAX_PACKETSIZE];
                char           szData[MAX_PACKETSIZE];
                unsigned long  lData[MAX_PACKETSIZE/4];
                float          fData[MAX_PACKETSIZE/4];
                sSender        Sender;
            } Data;                                 // Payload

        };
    //#pragma pack(pop) // End structures without padding

    enum Command {DATA, FRAME, TEST, PING};
    enum State {NOT_CONNECTED, CONNECTING, CONNECTED};

    explicit ClientCore(QObject *parent = 0);
    virtual ~ClientCore();
    int start(QString serverAddres, QString clientAddress);
    void disconnect();
    void sendCommand(Command command);

signals:

public slots:
    void commandReading();
    void dataReading();
    void waitForServer();

private:
    QUdpSocket commandSocket_;
    QUdpSocket dataSocket_;
    QHostAddress multicastAddress_;
    QHostAddress serverAddress_;     // server address for commands
    QHostAddress myAddress_;
    int commandPort_;
    int dataPort_;
    State state_;
    std::string appID;
    std::string serverName_;
    double lastTimestamp_;
    bool notFirstTimeLatency_;

    // Buggy server information
    struct buggyServer
    {
        std::string name;          // buggy sending app's name
        int version[4];  // buggy sending app's version [major.minor.build.revision]

    };
    std::list<buggyServer> buggyServers_;      // known buggy sending apps
    std::map<char,bool> fixBuggyServer_;        // whether or not the patch should be applied for that server


    void unpack(char *pData);
    void setVersions(sPacket &PacketIn);
    int NatNetVersion_[4];
    int ServerVersion_[4];

    bool decodeTimecode(unsigned int inTimecode,
                        unsigned int inTimecodeSubframe, int* hour, int* minute,
                        int* second, int* frame, int* subframe);
    bool timecodeStringify(unsigned int inTimecode,
                           unsigned int inTimecodeSubframe,
                           std::string &str);

    bool createSockets();
    void initCommunication();
    QNetworkInterface iFaceFromIP(QHostAddress address);
    void autoConnectToServer();
    void populateBuggyServers();
    void checkoForBuggyServers();
};

#endif // NETNAT_H
